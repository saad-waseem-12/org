---
title: Team Members 💼
description: Grey Software's Team Members
category: Team
position: 1000
currentMembers:
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
  - name: Raj
    avatar: https://gitlab.com/uploads/-/system/user/avatar/8089604/avatar.png
    position: Design Lead
    github: https://github.com/teccUI
    gitlab: https://gitlab.com/teccUI
    linkedin: https://www.linkedin.com/in/raj-paul-368827136/
  - name: Laiba Gul
    avatar: https://gitlab.com/uploads/-/system/user/avatar/8347443/avatar.png?width=400
    github: https://github.com/Laiba407
    gitlab: https://gitlab.com/Laiba407
    linkedin: https://www.linkedin.com/in/laiba-gul-797853206/
  - name: Faraz Ahmad Khan
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7891204/avatar.png
    github: https://github.com/farazahmadkhan15
    gitlab: https://gitlab.com/farazahmadkhan15
    linkedin: https://www.linkedin.com/in/farazahmadkhan15/
  - name: Saifullah Saif
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8335114/avatar.png
    github: https://github.com/saifullah111
    gitlab: https://gitlab.com/saifullah111
    linkedin: https://www.linkedin.com/in/saifullah111/
  - name: Zakir Ullah Bangash
    avatar: https://avatars.githubusercontent.com/u/79740254?s=460u=b7c2d910da903c65083755499fbca43b773c8473&v=4
    github: https://github.com/zakirBangashUETM
    gitlab: https://gitlab.com/ZakirBangash
    linkedin: https://linkedin.com/in/zakir-bangash-6b33a0199/

pastMembers:
  - name: Milind
    avatar: https://avatars.githubusercontent.com/u/48028572?s=460&u=7a0793800536db2a99dace6279495835f0550567&v=4
    position: Software Developer
    github: https://github.com/milindvishnoi
    gitlab: https://gitlab.com/milindvishnoi
    linkedin: https://www.linkedin.com/in/milindvishnoi/
  - name: Isha
    avatar: https://avatars.githubusercontent.com/u/56453971?s=460&v=4
    position: Software & Operations
    github: https://github.com/ishaaa-ai
    gitlab: https://gitlab.com/ishaaa-ai
    linkedin: https://www.linkedin.com/in/isha-kerpal-6a923819b/
  - name: Adil Shehzad
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7507821/avatar.png
    position: Software Developer
    github: adilshehzad786
    gitlab: adilshehzad
    linkedin: adilshehzad7
  - name: Hamees
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8058458/avatar.png
    position: Administrative Officer
    github: https://github.com/mhamees
    gitlab: https://gitlab.com/mhamees
    linkedin: https://www.linkedin.com/in/muhammad-hamees/
  - name: Avi Dave
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8512305/avatar.png?width=90
    position: Software Creator
    github: https://github.com/daveavi
    gitlab: https://gitlab.com/daveavi
    linkedin: https://www.linkedin.com/in/avi-dave-854715164/
---

## Active Team Members

<team-profiles :profiles="currentMembers"></team-profiles>

## Past Team Members

<team-profiles :profiles="pastMembers"></team-profiles>
