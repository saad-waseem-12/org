---
title: Certificate of Incorporation
description: Grey Software's official certificate of incorporation
category: Info
position: 6
---

You can download our certificate of incorporation by clicking the button below!

<cta-button text="Download Certificate" link="/certificate-of-incorporation.pdf"></cta-button>
