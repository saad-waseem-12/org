---
title: CAN 🇨🇦 UTM
description:
position: 4000
category: University Apprentices
alumni:
  - name: Milind
    avatar: https://avatars.githubusercontent.com/u/48028572?s=460&u=7a0793800536db2a99dace6279495835f0550567&v=4
    position: Software Developer
    github: https://github.com/milindvishnoi
    gitlab: https://gitlab.com/milindvishnoi
    linkedin: https://www.linkedin.com/in/milindvishnoi/
  - name: Isha
    avatar: https://avatars.githubusercontent.com/u/56453971?s=460&v=4
    position: Operations
    github: https://github.com/ishaaa-ai
    gitlab: https://gitlab.com/ishaaa-ai
    linkedin: https://www.linkedin.com/in/isha-kerpal-6a923819b/
---

# University of Toronto Mississauga

## Alumni

<team-profiles :profiles="alumni"></team-profiles>
